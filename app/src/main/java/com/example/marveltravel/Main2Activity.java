package com.example.marveltravel;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class Main2Activity extends AppCompatActivity {
    Context context = this;
    Spinner daftarTujuan;
    String[] listTujuan = {"Jakarta", "Palembang", "Kalimantan Barat"};
    int[] listHarga = {85000,150000,185000};
    TextView tanggalBerangkat, waktuBerangkat, saldo, topup,tanggalPulang,waktuPulang,kepulangan;
    EditText jumlah;
    Switch pulangPergi;
    String[]list;
    NumberFormat formatter;
    EditText saldoX;
    Dialog dialog;
    int tarifPulangPergi=1;

    //Current Saldo
    public static int currentSaldo = 250000;
    int checkOut;


    SimpleDateFormat dateFormatter;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        daftarTujuan = (Spinner) findViewById(R.id.spinner);
        list = new String[listTujuan.length];
        formatter = NumberFormat.getNumberInstance(new Locale("in","ID"));
        for (int i = 0;i<listTujuan.length;i++){
            list[i]=listTujuan[i]+"(Rp " + formatter.format(listHarga[i])+"/orang)";
        }

        final ArrayAdapter<String>adapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,list);
        daftarTujuan.setAdapter(adapter);

        saldo = findViewById(R.id.nominal);
        jumlah = findViewById(R.id.totaltik);
        pulangPergi = findViewById(R.id.pulper);
        tanggalBerangkat = findViewById(R.id.plhtgl);
        waktuBerangkat = findViewById(R.id.plhwkt);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tanggalPulang = findViewById(R.id.plhtgl1);
        kepulangan = findViewById(R.id.kepulangan);
        waktuPulang = findViewById(R.id.plhwkt1);

        //Secara Default tidak pake Kepulangan;
        setKepulangan(false);

        //SetSaldo Awal
        saldo.setText("Rp " + formatter.format(currentSaldo));

        //Listener untuk perubahan Check
        pulangPergi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setKepulangan(isChecked);
            }
        });
    }

    public void setKepulangan(boolean isChecked){
        if (isChecked){
            kepulangan.setVisibility(View.VISIBLE);
            tanggalPulang.setVisibility(View.VISIBLE);
            waktuPulang.setVisibility(View.VISIBLE);
            tarifPulangPergi=2;
        }else{
            tarifPulangPergi=1;
            kepulangan.setVisibility(View.GONE);
            tanggalPulang.setVisibility(View.GONE);
            waktuPulang.setVisibility(View.GONE);
        }
        tanggalPulang.setText("Pilih Tanggal");
        waktuPulang.setText("Pilih Waktu");
        for (int i = 0;i<listTujuan.length;i++){
            list[i]=listTujuan[i]+" (Rp " + formatter.format(listHarga[i]*tarifPulangPergi)+"/orang)";
        }
        final ArrayAdapter<String>adapter=new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,list);
        int sel = daftarTujuan.getSelectedItemPosition();
        daftarTujuan.setAdapter(adapter);
        daftarTujuan.setSelection(sel);
    }

    public void beliTiket(View view){
        if (jumlah.getText().toString().equalsIgnoreCase("")||Integer.parseInt(jumlah.getText().toString())<=0){
            Toast.makeText(this, "Masukkan jumlah Tiket", Toast.LENGTH_SHORT).show();
            jumlah.requestFocus();
        }else{
            checkOut = listHarga[daftarTujuan.getSelectedItemPosition()]*tarifPulangPergi*Integer.parseInt(jumlah.getText().toString());
            if (currentSaldo<checkOut) {
                Toast.makeText(this, "Saldo Anda tidak mencukupi, Silahkan Top up", Toast.LENGTH_SHORT).show();
            }else if(tanggalBerangkat.getText().toString().equalsIgnoreCase("Pilih Tanggal")){
                Toast.makeText(context, "Pilih Tanggal keberangkatan", Toast.LENGTH_SHORT).show();
            }else if(tanggalPulang.getText().toString().equalsIgnoreCase("Pilih Tanggal")&&pulangPergi.isChecked()){
                Toast.makeText(context, "Pilih Tanggal Kepulangan", Toast.LENGTH_SHORT).show();
            }else if(waktuBerangkat.getText().toString().equalsIgnoreCase("Pilih Waktu")){
                Toast.makeText(context, "Pilih Waktu Keberangkatan", Toast.LENGTH_SHORT).show();
            }else if(waktuPulang.getText().toString().equalsIgnoreCase("Pilih Waktu")&&pulangPergi.isChecked()){
                Toast.makeText(context, "Pilih Waktu Kepulangan", Toast.LENGTH_SHORT).show();
            }else {
                //Saldo Cukup dan segera ke summary
                Log.d("uhuy","test");
                Intent summary = new Intent(this, Main3Activity.class);
                summary.putExtra("tujuan", listTujuan[daftarTujuan.getSelectedItemPosition()]);
                summary.putExtra("tanggalBerangkat", tanggalBerangkat.getText().toString());
                summary.putExtra("waktuBerangkat", waktuBerangkat.getText().toString());
                summary.putExtra("pulangPergi", pulangPergi.isChecked());
                if (pulangPergi.isChecked()) {
                    summary.putExtra("tanggalPulang", tanggalPulang.getText().toString());
                    summary.putExtra("waktuPulang", waktuPulang.getText().toString());
                }
                summary.putExtra("total", checkOut);
                summary.putExtra("jumlahTiket", jumlah.getText().toString());
                startActivity(summary);
                Log.d("uhuy","YEay");
//                onPause();
                Toast.makeText(this, "" + formatter.format(checkOut), Toast.LENGTH_SHORT).show();

            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!saldo.getText().toString().equalsIgnoreCase("Rp " + formatter.format(currentSaldo))){
            tanggalBerangkat.setText("Pilih Tanggal");
            tanggalPulang.setText("Pilih Tanggal");
            waktuBerangkat.setText("Pilih Waktu");
            waktuPulang.setText("Pilih Waktu");
            jumlah.setText("");
            setKepulangan(false);
        }
        refreshSaldo();
    }

    protected void refreshSaldo(){
        NumberFormat num = NumberFormat.getNumberInstance(new Locale("in","ID"));
        saldo.setText("Rp " + num.format(currentSaldo));
    }

    public void topup(View view){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_saldo);
        TextView tambahSaldo, batal;
        saldoX = dialog.findViewById(R.id.edSaldo);

        tambahSaldo=dialog.findViewById(R.id.txTambahSaldo);
        batal = dialog.findViewById(R.id.txBatal);

        tambahSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(saldoX.getText().toString())>5000000){
                    Toast.makeText(context, "Batas Top up melebihi Rp " + formatter.format(5000000) , Toast.LENGTH_SHORT).show();
                    saldoX.requestFocus();
                }else {
                    currentSaldo += Integer.parseInt(saldoX.getText().toString());
                    saldo.setText("Rp " + formatter.format(currentSaldo));
                    dialog.dismiss();
                }
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void pilihTanggal(View view){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                tanggalBerangkat.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void pilihTanggalPulang(View view){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                tanggalPulang.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void pilihWaktu(View view){
        Calendar calendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                waktuBerangkat.setText(hourOfDay+":"+minute);
            }
        },calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    public void pilihWaktuPulang(View view){
        Calendar calendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                waktuPulang.setText(hourOfDay+":"+minute);
            }
        },calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
}
