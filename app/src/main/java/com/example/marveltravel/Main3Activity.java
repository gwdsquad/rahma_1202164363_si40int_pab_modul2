package com.example.marveltravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class Main3Activity extends AppCompatActivity {
    TextView tujuan,tanggalBerangkat,tanggalPulang,harga,jumlahTiket,txPulang;
    NumberFormat numberFormat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        tujuan = findViewById(R.id.dstn);
        tanggalBerangkat = findViewById(R.id.tglbrg1);
        tanggalPulang = findViewById(R.id.tglplg1);
        jumlahTiket = findViewById(R.id.jumtiket);
        harga = findViewById(R.id.totalnya);
        txPulang = findViewById(R.id.tglplg);
        if (!getIntent().getBooleanExtra("pulangPergi",false)){
            tanggalPulang.setVisibility(View.GONE);
            txPulang.setVisibility(View.GONE);
        }else{
            tanggalPulang.setText(getIntent().getStringExtra("tanggalPulang")+" " + getIntent().getStringExtra("waktuPulang")+" WIB");
        }
        tujuan.setText(getIntent().getStringExtra("tujuan"));
        tanggalBerangkat.setText(getIntent().getStringExtra("tanggalBerangkat")+" " + getIntent().getStringExtra("waktuBerangkat")+" WIB");
        jumlahTiket.setText(getIntent().getStringExtra("jumlahTiket"));
        numberFormat = NumberFormat.getNumberInstance(new Locale("in","ID"));

        harga.setText("Rp " + numberFormat.format(getIntent().getIntExtra("total",0)));

    }

    public void confirm(View view){
        Main2Activity.currentSaldo -= getIntent().getIntExtra("total",0);
        finish();

    }
}
